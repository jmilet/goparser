package plugin

import (
	"encoding/json"
	"fmt"
)

type Payload struct {
	Ref          string `json:"reg"`
	RefType      string `json:"reg_type"`
	MasterBranch string `json:"master_branch"`
	Description  string `json:"description"`
	PusherType   string `json:"pusher_type"`
}

type Repo struct {
	ID   int    `json:"id"`
	Name string `json:"name"`
	URL  string `json:"url"`
}

type Actor struct {
	ID         int    `json:"id"`
	Login      string `json:"login"`
	GravatarID string `json:"gravatar_id"`
	URL        string `json:"url"`
	AvatarURL  string `json:"avatar_url"`
}

type Reg struct {
	ID        string  `json:"id"`
	Type      string  `json:"type"`
	Actor     Actor   `json:"actor"`
	Tepo      Repo    `json:"repo"`
	Payload   Payload `json:"payload"`
	Public    bool    `json:"public"`
	CreatedAt string  `json:"created_at"`
}

type PluginUno struct {
	PluginDummy
}

func init() {
	Register("uno", &PluginUno{})
}

func (p *PluginUno) ProcessLine(text string, workerNum int) []string {
	var reg Reg

	if err := json.Unmarshal([]byte(text), &reg); err != nil {
		panic(err)
	}
	return []string{fmt.Sprintf("%s - %s", reg.Actor.AvatarURL, reg.CreatedAt)}
}
