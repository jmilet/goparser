package plugin

import "strings"

type PluginDummy struct{}

func init() {
	Register("dummy", &PluginDummy{})
}

func (p *PluginDummy) InitPlugin(pluginNam string, numWorkers int) {
}

func (p *PluginDummy) FinishPlugin(pluginNam string, numWorkers int) {
}

func (p *PluginDummy) ProcessLine(text string, workerNum int) []string {
	return strings.Split(text, ",")
}
