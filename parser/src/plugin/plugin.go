package plugin

type PluginFunc func(string) []string

type Plugin interface {
	InitPlugin(pluginName string, numWorkers int)
	FinishPlugin(plutginName string, numWorkers int)
	ProcessLine(text string, workerNum int) []string
}

var plugins map[string]*Plugin

func init() {
	plugins = make(map[string]*Plugin)
}

func Register(pluginName string, plugin Plugin) {
	plugins[pluginName] = &plugin
}

func GetPluginFor(name string) *Plugin {
	plugin := plugins[name]
	return plugin
}
