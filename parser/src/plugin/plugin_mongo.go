package plugin

import (
	"encoding/json"
	"log"

	"fmt"

	"gopkg.in/mgo.v2"
)

type record struct {
	ID        string `json:"id"`
	Type      string `json:"type"`
	Public    bool   `json:"public"`
	CreatedAt string `json:"created_at"`
}

type connexion struct {
	session    *mgo.Session
	collection *mgo.Collection
}

type PluginMongo struct {
	conns []connexion
}

func init() {
	Register("mongo", &PluginMongo{})
}

func (p *PluginMongo) InitPlugin(pluginNam string, numWorkers int) {
	var err error

	fmt.Println("init")

	for i := 0; i < numWorkers; i++ {
		conn := connexion{}
		conn.session, err = mgo.Dial("mongodb://192.168.1.52")
		if err != nil {
			panic(err)
		}

		// Optional. Switch the session to a monotonic behavior.
		conn.session.SetMode(mgo.Monotonic, true)
		conn.collection = conn.session.DB("test").C("data")
		p.conns = append(p.conns, conn)
	}
}

func (p *PluginMongo) FinishPlugin(pluginName string, numWorkers int) {
	fmt.Println("finish")
	for _, conn := range p.conns {
		conn.session.Close()
	}
}

func (p *PluginMongo) ProcessLine(text string, workerNum int) []string {
	var err error
	var rec record

	//fmt.Println(text)
	if err = json.Unmarshal([]byte(text), &rec); err != nil {
		panic(err)
	}
	//fmt.Println(rec)

	err = p.conns[workerNum].collection.Insert(&rec)
	if err != nil {
		log.Fatal(err)
	}

	return []string{}
}
