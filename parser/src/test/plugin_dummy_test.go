package dummy_plugin_test

import (
	"plugin"
	"testing"
)

func TestPluginDummy(t *testing.T) {
	dummy := plugin.PluginDummy{}
	ret := dummy.ProcessLine("hola,que,tal")

	if ret[0] != "hola" {
		t.Error("Expected hola")
	}

	if ret[1] != "que" {
		t.Error("Expected que")
	}

	if ret[2] != "tal" {
		t.Error("Expected tal")
	}
}
