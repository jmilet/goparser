package main

import (
	"bufio"
	"fmt"
	"io"
	"os"
	"plugin"
	"strconv"
	"sync"
)

var wg sync.WaitGroup

func transformer(plugin *plugin.Plugin, input chan string, output chan []string, workerNum int) {
	defer wg.Done()

	for line := range input {
		output <- (*plugin).ProcessLine(line, workerNum)
	}
}

func producer(reader *bufio.Reader, input chan string) {
	defer close(input)
	defer wg.Done()

	line, err := reader.ReadString('\n')
	for err == nil {
		input <- line
		line, err = reader.ReadString('\n')
	}
	if err != nil && err != io.EOF {
		fmt.Println(err)
		os.Exit(-1)
	}
}

func dumper(output chan []string) {
	for lines := range output {
		for _, line := range lines {
			fmt.Println(line)
		}
	}
}

func main() {
	pluginName := os.Args[2]
	numberOfWorkers, err := strconv.Atoi(os.Args[3])
	if err != nil {
		fmt.Println("Error number of workers")
		os.Exit(-1)
	}

	file, err := os.Open(os.Args[1])
	if err != nil {
		panic(err)
	}
	defer file.Close()

	// Setup plugin.
	plugin := plugin.GetPluginFor(pluginName)
	(*plugin).InitPlugin(pluginName, numberOfWorkers)

	reader := bufio.NewReader(file)
	input := make(chan string, 10000)
	output := make(chan []string, 100000)

	wg.Add(numberOfWorkers + 1)
	go producer(reader, input)
	for i := 0; i < numberOfWorkers; i++ {
		go transformer(plugin, input, output, i)
	}
	go func() {
		wg.Wait()
		close(output)
	}()

	dumper(output)

	// Finish plugin.
	(*plugin).FinishPlugin(pluginName, numberOfWorkers)

	os.Exit(0)
}
